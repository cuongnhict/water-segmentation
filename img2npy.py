import warnings
import cv2
import os
import numpy as np

warnings.filterwarnings('ignore')

train_dir = os.path.join(os.getcwd(), 'dataset', 'water_segmentation_training')
validation_dir = os.path.join(os.getcwd(), 'dataset', 'water_segmentation_validation')
test_dir = os.path.join(os.getcwd(), 'dataset', 'water_segmentation_test')
train_data_file_path = os.path.join(train_dir, 'data.txt')
validation_data_file_path = os.path.join(validation_dir, 'data.txt')
test_data_file_path = os.path.join(test_dir, 'data.txt')
train_image_npy_file_path = os.path.join(os.getcwd(), 'dataset', 'train_image.npy')
train_mask_npy_file_path = os.path.join(os.getcwd(), 'dataset', 'train_mask.npy')
validation_image_npy_file_path = os.path.join(os.getcwd(), 'dataset', 'validation_image.npy')
validation_mask_npy_file_path = os.path.join(os.getcwd(), 'dataset', 'validation_mask.npy')
test_image_npy_file_path = os.path.join(os.getcwd(), 'dataset', 'test_image.npy')
test_mask_npy_file_path = os.path.join(os.getcwd(), 'dataset', 'test_mask.npy')


def get_images_and_masks(water_dir):
    images = []
    masks = []

    with open(os.path.join(water_dir, 'data.txt')) as file:
        content = file.readlines()

    for line in content:
        values = line.strip().split(' ')
        image_file_path = os.path.join(water_dir, values[0])
        mask_file_path = os.path.join(water_dir, values[1])

        image = cv2.imread(image_file_path)
        mask = cv2.imread(mask_file_path)
        mask = cv2.cvtColor(mask, cv2.COLOR_BGR2GRAY)

        image = cv2.resize(image, (384, 384))
        mask = cv2.resize(mask, (384, 384))

        images.append(image)
        masks.append(mask)

    images = np.array(images)
    masks = np.array(masks)
    masks = np.expand_dims(masks, axis=3)

    return images, masks


train_images, train_masks = get_images_and_masks(train_dir)
validation_images, validation_masks = get_images_and_masks(validation_dir)
test_images, test_masks = get_images_and_masks(test_dir)

np.save(train_image_npy_file_path, train_images)
np.save(train_mask_npy_file_path, train_masks)
np.save(validation_image_npy_file_path, validation_images)
np.save(validation_mask_npy_file_path, validation_masks)
np.save(test_image_npy_file_path, test_images)
np.save(test_mask_npy_file_path, test_masks)

print('Saved!')
