import warnings
import os
import numpy as np
from unet_model import unet
from keras.callbacks import TensorBoard, ModelCheckpoint, EarlyStopping
from keras.optimizers import Adam

warnings.filterwarnings('ignore')

X_train = np.load(os.path.join(os.getcwd(), 'dataset', 'train_image.npy'))
y_train = np.load(os.path.join(os.getcwd(), 'dataset', 'train_mask.npy'))
X_val = np.load(os.path.join(os.getcwd(), 'dataset', 'validation_image.npy'))
y_val = np.load(os.path.join(os.getcwd(), 'dataset', 'validation_mask.npy'))

X_train = X_train / 255.0
y_train = y_train / 255.0
X_val = X_val / 255.0
y_val = y_val / 255.0

callbacks = [
    EarlyStopping(min_delta=1e-4, patience=10, verbose=1),
    TensorBoard(log_dir='logs/', batch_size=8, write_graph=True),
    ModelCheckpoint('unet_100epochs.h5', monitor='val_loss', verbose=1, save_best_only=True)
]

model = unet()
model.compile(optimizer=Adam(lr=1e-4), loss='binary_crossentropy', metrics=['accuracy'])
model.fit(X_train, y_train, epochs=100, callbacks=callbacks, validation_data=(X_val, y_val))
